package cn.vonce.validator.model;

/**
 * @author Jovi
 * @version 1.0
 * @email imjovi@qq.com
 * @date 2020/1/21 17:28
 */
public class FieldTarget {

    private String name;
    private String tips;
    private Object value;
    private Object bean;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    @Override
    public String toString() {
        return "FieldTarget{" +
                "name='" + name + '\'' +
                ", tips='" + tips + '\'' +
                ", value=" + value +
                ", bean=" + bean +
                '}';
    }
}
