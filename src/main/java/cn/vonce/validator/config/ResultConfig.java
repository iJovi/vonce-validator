package cn.vonce.validator.config;

import cn.vonce.validator.enumerate.ResultType;
/**
 * 校验结果配置
 *
 * @author Jovi
 * @email imjovi@qq.com
 * @date 2022/5/9 20:25
 */
public class ResultConfig {

    private ResultType resultType = ResultType.RETURN_TIPS;
    private String codeFieldName;
    private String msgFieldName;
    private Object resultTemplate;

    public ResultType getResultType() {
        return resultType;
    }

    public void setResultType(ResultType resultType) {
        this.resultType = resultType;
    }

    public String getCodeFieldName() {
        return codeFieldName;
    }

    public void setCodeFieldName(String codeFieldName) {
        this.codeFieldName = codeFieldName;
    }

    public String getMsgFieldName() {
        return msgFieldName;
    }

    public void setMsgFieldName(String msgFieldName) {
        this.msgFieldName = msgFieldName;
    }

    public Object getResultTemplate() {
        return resultTemplate;
    }

    public void setResultTemplate(Object resultTemplate) {
        this.resultTemplate = resultTemplate;
    }
}
