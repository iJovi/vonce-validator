package cn.vonce.validator.annotation;

import cn.vonce.validator.rule.impl.ValidateNotBlank;
import java.lang.annotation.*;

/**
 * 校验字段不能为blank
 *
 * @author Jovi
 * @version 1.0
 * @email imjovi@qq.com
 * @date 2022/6/8 20:34
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Documented
@Validate(type = ValidateNotBlank.class)
public @interface VNotBlank {

    /**
     * 字段名称
     *
     * @return
     */
    String name() default "";

    /**
     * 消息提示
     *
     * @return
     */
    String value() default "";

    /**
     * 分组校验
     *
     * @return
     */
    String[] group() default "";

}
