package cn.vonce.validator.result;

import java.util.HashMap;

/**
 * 默认的参数校验返回结果规范
 *
 * @author Jovi
 * @email imjovi@qq.com
 * @date 2022/5/9 17:12
 */
public class DefaultStruct extends HashMap<String, Object> {

    public void setCode(Integer code) {
        super.put("code", code);
    }

    public Integer getCode() {
        return (Integer) super.get("code");
    }

    public void setMsg(String msg) {
        super.put("msg", msg);
    }

    public String getMsg() {
        return (String) super.get("msg");
    }

}
