//package cn.vonce.validator.result;
//
///**
// * 参数校验返回结果结构
// *
// * @author Jovi
// * @email imjovi@qq.com
// * @date 2022/5/9 17:10
// */
//public interface Struct {
//
//    void setCode(Integer code);
//
//    Integer getCode();
//
//    void setMsg(String msg);
//
//    String getMsg();
//
//    void setData(Object data);
//
//    Object getData();
//
//}
//
////如果模板存在则根据模板类创建实例并赋值，如果结果模板为null则创建默认返回结构
//            if (validatorConfig != null) {
//                    result = validatorConfig.getStruct().getClass().newInstance();
//                    result.setCode(validatorConfig.getStruct().getCode());
//                    result.setMsg(validatorConfig.getStruct().getMsg());
//                    result.setData(validatorConfig.getStruct().getData());
//                    } else {
//                    result = new DefaultStruct();
//                    }
//                    if (result.getCode() == null) {
//                    result.setCode(ResultCode.PARAMETER.getCode());
//                    }
//                    if (result.getMsg() == null) {
//                    result.setMsg(tips);
//                    }
//                    //如果用户配置了 并且 用户配置的是返回提示
//                    if (validatorConfig == null || (validatorConfig != null && validatorConfig.getResultType() == ResultType.RETURN_TIPS)) {
//                    return result;
//                    }
//                    throw new ValidatorException("参数校验失败：" + tips);
