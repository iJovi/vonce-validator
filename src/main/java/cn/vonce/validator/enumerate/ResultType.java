package cn.vonce.validator.enumerate;

/**
 * 结果类型（RETURN_TIPS：返回提示，THROW_TIPS抛出提示）
 *
 * @author Jovi
 * @email imjovi@qq.com
 * @date 2022/5/9 20:26
 */
public enum ResultType {

    RETURN_TIPS, THROW_TIPS

}
