package cn.vonce.validator.rule;

import cn.vonce.validator.helper.ValidatorHelper;
import cn.vonce.validator.helper.WhatType;
import cn.vonce.validator.model.FieldTarget;
import cn.vonce.validator.model.FieldResult;
import cn.vonce.validator.utils.ValidatorUtil;

import java.lang.annotation.Annotation;

/**
 * 校验规则抽象实现
 *
 * @author Jovi
 * @version 1.0
 * @email imjovi@qq.com
 * @date 2020/1/21 17:01
 */
public abstract class AbstractValidate<T extends Annotation> implements ValidateRule<T> {

    public abstract WhatType[] type();

    public abstract String getAnticipate(T valid);

    public abstract boolean onlyWhenNotEmpty(T valid);

    public abstract boolean check(T valid, FieldTarget fieldTarget);

    @Override
    public FieldResult handle(T valid, FieldTarget fieldTarget) {
        String tips = ValidatorUtil.getTips(fieldTarget.getName(), fieldTarget.getTips(), getAnticipate(valid));
        if (ValidatorUtil.isNeedValidation(onlyWhenNotEmpty(valid), fieldTarget.getValue())) {
            if (fieldTarget.getValue() == null) {
                return new FieldResult(fieldTarget.getName(), tips, ValidatorUtil.getNullError());
            }
            WhatType thisType = ValidatorHelper.whatType(fieldTarget.getValue().getClass().getSimpleName());
            boolean accordWith = false;
            WhatType[] whatTypes = type();
            if (whatTypes != null) {
                for (WhatType whatType : whatTypes) {
                    if (thisType == whatType) {
                        accordWith = true;
                        break;
                    }
                }
            }
            if (!accordWith) {
                return new FieldResult(fieldTarget.getName(), tips, ValidatorUtil.getTypeError(whatTypes));
            }
            if (!check(valid, fieldTarget)) {
                return new FieldResult(fieldTarget.getName(), tips, ValidatorUtil.getAnticipateError(getAnticipate(valid)));
            }
        }
        return new FieldResult(true, fieldTarget.getName());
    }

}
