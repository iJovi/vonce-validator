package cn.vonce.validator.rule.impl;

import cn.vonce.validator.annotation.VRegex;
import cn.vonce.validator.helper.WhatType;
import cn.vonce.validator.model.FieldTarget;
import cn.vonce.validator.rule.AbstractValidate;
import java.util.regex.Pattern;

/**
 * 校验是否符合正则表达式的规则
 *
 * @author Jovi
 * @version 1.0
 * @email imjovi@qq.com
 * @date 2020/1/21 15:51
 */
public class ValidateRegex extends AbstractValidate<VRegex> {

    @Override
    public WhatType[] type() {
        return new WhatType[]{WhatType.STRING_TYPE};
    }

    @Override
    public String getAnticipate(VRegex valid) {
        return null;
    }

    @Override
    public boolean onlyWhenNotEmpty(VRegex valid) {
        return false;
    }

    @Override
    public boolean check(VRegex valid, FieldTarget fieldTarget) {
        if (!Pattern.matches(valid.val(), fieldTarget.getValue().toString())) {
            return false;
        }
        return true;
    }

}
