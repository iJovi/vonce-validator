package cn.vonce.validator.rule.impl;

import cn.vonce.validator.utils.ValidatorRuleUtil;
import cn.vonce.validator.annotation.VSQLInject;
import cn.vonce.validator.helper.WhatType;
import cn.vonce.validator.model.FieldTarget;
import cn.vonce.validator.rule.AbstractValidate;

/**
 * 校验sql注入
 *
 * @author Jovi
 * @version 1.0
 * @email imjovi@qq.com
 * @date 2020/1/19 15:18
 */
public class ValidateSQLInject extends AbstractValidate<VSQLInject> {

    @Override
    public WhatType[] type() {
        return new WhatType[]{WhatType.STRING_TYPE};
    }

    @Override
    public String getAnticipate(VSQLInject valid) {
        return "'标准Sql语句'";
    }

    @Override
    public boolean onlyWhenNotEmpty(VSQLInject valid) {
        return valid.onlyWhenNotEmpty();
    }

    @Override
    public boolean check(VSQLInject valid, FieldTarget fieldTarget) {
        if (!ValidatorRuleUtil.isSQLInject(fieldTarget.getValue().toString())) {
            return false;
        }
        return true;
    }

}
