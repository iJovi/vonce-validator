package cn.vonce.validator.rule.impl;

import cn.vonce.validator.utils.ValidatorRuleUtil;
import cn.vonce.validator.annotation.VPassword;
import cn.vonce.validator.helper.WhatType;
import cn.vonce.validator.model.FieldTarget;
import cn.vonce.validator.rule.AbstractValidate;

/**
 * 校验密码
 *
 * @author Jovi
 * @version 1.0
 * @email imjovi@qq.com
 * @date 2020/1/19 15:27
 */
public class ValidatePassword extends AbstractValidate<VPassword> {

    @Override
    public WhatType[] type() {
        return new WhatType[]{WhatType.STRING_TYPE};
    }

    @Override
    public String getAnticipate(VPassword valid) {
        return "'标准密码格式'";
    }

    @Override
    public boolean onlyWhenNotEmpty(VPassword valid) {
        return valid.onlyWhenNotEmpty();
    }

    @Override
    public boolean check(VPassword valid, FieldTarget fieldTarget) {
        if (!ValidatorRuleUtil.isPassword(fieldTarget.getValue().toString())) {
            return false;
        }
        return true;
    }

}
