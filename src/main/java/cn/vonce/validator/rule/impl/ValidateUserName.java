package cn.vonce.validator.rule.impl;

import cn.vonce.validator.utils.ValidatorRuleUtil;
import cn.vonce.validator.annotation.VUserName;
import cn.vonce.validator.helper.WhatType;
import cn.vonce.validator.model.FieldTarget;
import cn.vonce.validator.rule.AbstractValidate;

/**
 * 校验用户名
 *
 * @author Jovi
 * @version 1.0
 * @email imjovi@qq.com
 * @date 2020/1/19 15:28
 */
public class ValidateUserName extends AbstractValidate<VUserName> {

    @Override
    public WhatType[] type() {
        return new WhatType[]{WhatType.STRING_TYPE};
    }

    @Override
    public String getAnticipate(VUserName valid) {
        return "'标准用户名格式'";
    }

    @Override
    public boolean onlyWhenNotEmpty(VUserName valid) {
        return valid.onlyWhenNotEmpty();
    }

    @Override
    public boolean check(VUserName valid, FieldTarget fieldTarget) {
        if (!ValidatorRuleUtil.isUsername(fieldTarget.getValue().toString())) {
            return false;
        }
        return true;
    }

}
