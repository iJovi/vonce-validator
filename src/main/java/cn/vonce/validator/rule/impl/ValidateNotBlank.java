package cn.vonce.validator.rule.impl;

import cn.vonce.validator.annotation.VNotBlank;
import cn.vonce.validator.helper.WhatType;
import cn.vonce.validator.model.FieldTarget;
import cn.vonce.validator.rule.AbstractValidate;

/**
 * 校验字段不能为blank
 *
 * @author Jovi
 * @version 1.0
 * @email imjovi@qq.com
 * @date 2022/6/8 20:34
 */
public class ValidateNotBlank extends AbstractValidate<VNotBlank> {

    @Override
    public WhatType[] type() {
        return new WhatType[]{WhatType.STRING_TYPE};
    }

    @Override
    public String getAnticipate(VNotBlank valid) {
        return "'不能为empty'";
    }

    @Override
    public boolean onlyWhenNotEmpty(VNotBlank valid) {
        return false;
    }

    @Override
    public boolean check(VNotBlank valid, FieldTarget fieldTarget) {
        String value = fieldTarget.getValue().toString();
        if ("".equals(value)) {
            return false;
        }
        int count = 0;
        CharSequence cs = value;
        for (int i = 0; i < value.length(); ++i) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                count++;
                break;
            }
        }
        return count > 0;
    }

}
